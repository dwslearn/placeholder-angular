/* 
    Imports
*/
    // Angular component
    import { Routes } from "@angular/router";

    // App component
    import { DashboardViewComponent } from "./views/dashboard-view/dashboard-view.component";
    import { HomeViewComponent } from "./views/home-view/home-view.component";
    import { SingleViewComponent } from "./views/single-view/single-view.component";
//

/* 
    Exports
*/
    export const AppRouterModule: Routes = [
        {
            path: '',
            component: HomeViewComponent
        },
        {
            path: 'dashboard',
            component: DashboardViewComponent,
        },
        {
            path: ':endpoint/:id',
            component: SingleViewComponent,
        }
    ]
//