/* 
  Imports
*/
// Angular components
import { Injectable } from '@angular/core';

// RxJS module
import { BehaviorSubject, Observable } from "rxjs";
//

/* 
  Definition and export
*/
  @Injectable({
    providedIn: 'root'
  })
  export class StateService {
    constructor() { }

    /* 
      [STATE] Definition
      Define protected values
    */
      protected fetchresponse: BehaviorSubject<any> = new BehaviorSubject<any>(null)
      
      protected authinfo: BehaviorSubject<any> = new BehaviorSubject<any>(
        JSON.parse( localStorage.getItem('authinfo') ) || null
      )
      
      protected albumlist: BehaviorSubject<[object]> = new BehaviorSubject<[object]>(
        JSON.parse( localStorage.getItem('albumlist') ) || null
      )
      protected singleitem: BehaviorSubject<[object]> = new BehaviorSubject<[object]>(
        JSON.parse( localStorage.getItem('singleitem') ) || null
      )
    //

    /* 
      [GETTER] Definition
      Define method to get state values
    */
      public getFetchresponse(): Observable<any>{ return this.fetchresponse }
      public getAuthinfo(): Observable<any>{ return this.authinfo }
      public getAlbumlist(): Observable<[object]>{ return this.albumlist }
      public getSingleitem(): Observable<[object]>{ return this.singleitem }
    //

    /* 
      [SETTER] Definition
      Definie global method to set state value
    */
      public setStateValue( state: string, value: any ){
        // Check state
        switch( state ){
          case 'fetchresponse': 
            return this.fetchresponse.next( value )

          case 'authinfo': 
            // Check value for persistance
            if(value !== null){ localStorage.setItem('authinfo', JSON.stringify( value )) }
            else{
              // Remove all user info on logout
              localStorage.removeItem('authinfo');
              localStorage.removeItem('albumlist');
              localStorage.removeItem('singleitem');
            }

            // Update state value
            return this.authinfo.next( value )

          case 'albumlist': 
            // Check value for persistance
            value !== null
            ? localStorage.setItem('albumlist', JSON.stringify( value ))
            : localStorage.removeItem('albumlist');

            // Update state value
            return this.albumlist.next( value )

          case 'singleitem': 
            // Check value for persistance
            value !== null
            ? localStorage.setItem('singleitem', JSON.stringify( value ))
            : localStorage.removeItem('singleitem');

            // Update state value
            return this.singleitem.next( value )

          default: return;
        }
      }
    //
  }
//