/* 
  Imports
*/
  // Angular component
  import { Injectable } from '@angular/core';
  import { HttpClient } from "@angular/common/http";

  // App component
  import { StateService } from "../state/state.service";
//

/* 
  Definition and exports
*/
  @Injectable({
    providedIn: 'root'
  })
  export class CrudService {

    /* 
      [CMP] Injection
      Inject value inside the class
    */
      constructor( private HttpClient: HttpClient, private StateService: StateService ) { }
    //

    public createOperation( data: object ){}
    
    /* 
      [CRUD] Read operation
      Send AJAX request GET method
    */
      public readOperation( data: { endpoint: string, state: string } ): Promise<any> {
        return this.HttpClient.get(`https://jsonplaceholder.typicode.com/${data.endpoint}`).toPromise()
        .then( jsonResponse => this.extractSuccessResponse(jsonResponse, data.state, 'readOperation', data.endpoint))
        .catch( errorResponse => this.extractErrorResponse(errorResponse, data.state, 'readOperation', data.endpoint))
      }
    //

    public updateOperation( data: object ){}
    public deleteOperation( data: object ){}

    /* 
      [CMP] Helpers
      Methods used in the main methods
    */
      private extractSuccessResponse( jsonResponse: any, state: string, action: string, endpoint: string ): any {
        /* 
          [SET] State
          Use state service to set state value
        */
          // Update fetchresponse to display BaseFlashnote
          this.StateService.setStateValue( 'fetchresponse', {
            ok: true,
            action: action,
            endpoint: endpoint,
            value: jsonResponse
          })

          // Update specific state
          this.StateService.setStateValue( state, jsonResponse )
        //

        // Resolve HTTP request SUCCESS
        return Promise.resolve( jsonResponse || [] )
      }
      
      private extractErrorResponse( errorResponse: any, state: string, action: string, endpoint: string ): any {
        // Update fetchresponse to display BaseFlashnote
        this.StateService.setStateValue( 'fetchresponse', {
          ok: false,
          action: action,
          endpoint: endpoint,
          value: errorResponse
        } )

        // Resolve HTTP request ERROR
        return Promise.reject( errorResponse || [] )
      }
    //
  }
//