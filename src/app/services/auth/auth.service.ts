/* 
  Imports
*/
  // Angular component
  import { Injectable } from '@angular/core';
  import { HttpClient } from "@angular/common/http";

  // App component
  import { StateService } from "../state/state.service";
//

/* 
  Definition and exports
*/
  @Injectable({
    providedIn: 'root'
  })
  export class AuthService {

    /* 
      [CMP] Injection
      Inject value inside the class
    */
      constructor( private HttpClient: HttpClient, private StateService: StateService ) { }
    //

    /* 
      [CRUD] Login operation
      Send AJAX request to log user
    */
      public loginOperation( data: { email: string, id: number } ): Promise<any> {
        return this.HttpClient.get(`https://jsonplaceholder.typicode.com/users?email=${data.email}&id=${data.id}`).toPromise()
        .then( jsonResponse => this.extractSuccessResponse(
          jsonResponse, 
          'authinfo', 
          'loginOperation', 
          `users?email=${data.email}&id=${data.id}`
        ))
        .catch( errorResponse => this.extractErrorResponse(
          errorResponse, 
          'authinfo', 
          'loginOperation', 
          `users?email=${data.email}&id=${data.id}`
        ))
      }
    //

    /* 
      [CRUD] Logut operation
      Define function to log out user
    */
    //

    /* 
      [CMP] Helpers
      Methods used in the main methods
    */
      private extractSuccessResponse( jsonResponse: any, state: string, action: string, endpoint: string ): any {
        console.log('extractSuccessResponse', jsonResponse)
        /* 
          [SET] State
          Use state service to set state value
        */
          // Update fetchresponse to display BaseFlashnote
          this.StateService.setStateValue( 'fetchresponse', {
            ok: true,
            action: action,
            endpoint: endpoint,
            value: jsonResponse
          })

          // Update specific state
          switch(state){
            case 'authinfo': this.StateService.setStateValue( state, jsonResponse[0] ); break;
            default:  this.StateService.setStateValue( state, jsonResponse[0] ); break;
          }
        //

        // Resolve HTTP request SUCCESS
        return Promise.resolve( jsonResponse || [] )
      }
      
      private extractErrorResponse( errorResponse: any, state: string, action: string, endpoint: string ): any {
        // Update fetchresponse to display BaseFlashnote
        this.StateService.setStateValue( 'fetchresponse', {
          ok: false,
          action: action,
          endpoint: endpoint,
          value: errorResponse
        } )

        // Resolve HTTP request ERROR
        return Promise.reject( errorResponse || [] )
      }
    //
  }
//