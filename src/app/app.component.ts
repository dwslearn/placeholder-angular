import { Component } from '@angular/core';
import { StateService } from "./services/state/state.service";
import { Router } from "@angular/router";

@Component({
  selector: 'app-root',
  template: `
    <!-- Display child component -->
    <app-main-header (onLogout)="onLogout()"></app-main-header>

    <!-- Display child component -->
    <app-base-flashnote
      (onToggle)="onToggleFlashnote($event)"
    ></app-base-flashnote>
    
    <!-- Use the directive to display route componeent in application shell -->
    <main class="p-2">
      <router-outlet></router-outlet>
    </main>

    <!-- 
      TODO: Create 'app-main-footer' and display album list length for connected user
    -->
    <footer class="has-background-primary p-2 is-size-6 is-uppercase has-text-white">Under WTFPL &copy; 2022</footer>
  `
})
export class AppComponent {
  /* 
    [CMP] Injection
    Inject value inside the class
  */
    constructor( private StateService: StateService, private Router: Router ) {
      // Subscribe to state observable
      this.StateService.getAuthinfo().subscribe( ( observer: object ) => {
        // Check if observer if null to redirect user
        if( observer === null ){ this.Router.navigate(['/']) }
      })
    }
  //

  /* 
    [CMP] Methods
    Define component methods
  */
    // Bind 'onToggle' event on 'app-base-flashnote'
    public onToggleFlashnote( event: Boolean ): void {
      // Update state value to display flashnoye
      this.StateService.setStateValue('fetchresponse', {
        ok: true,
        action: 'closeflashnote',
        endpoint: undefined,
        value: null
      })
    }

    // Bind 'onLogout' event on 'app-main-header'
    public onLogout(): void {
      // Logout user from StateService
      this.StateService.setStateValue( 'authinfo', null )
    }
  //
}
