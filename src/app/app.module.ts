/* 
  Imports
*/
  // Angular components
  import { BrowserModule } from '@angular/platform-browser';
  import { NgModule } from '@angular/core';

  // Module to send HTTP request
  import { HttpClientModule } from "@angular/common/http";

  // Module to manage forms
  import { ReactiveFormsModule } from "@angular/forms";
  import { FormsModule } from "@angular/forms";

  // Router components
  import { RouterModule } from "@angular/router";
  import { AppRouterModule } from "./app.router";

  // App compoentns
  import { AppComponent } from './app.component';
  import { DashboardViewComponent } from './views/dashboard-view/dashboard-view.component';
  import { SingleViewComponent } from './views/single-view/single-view.component';
  import { BasePushAlbumComponent } from './components/base/base-push-album/base-push-album.component';
  import { BaseFlashnoteComponent } from './components/base/base-flashnote/base-flashnote.component';
  import { HomeViewComponent } from './views/home-view/home-view.component';
  import { BaseFormComponent } from './components/base/base-form/base-form.component';
import { MainHeaderComponent } from './components/main/main-header/main-header.component';
//

/* 
  Definition and export
*/
  @NgModule({
    declarations: [
      AppComponent,
      DashboardViewComponent,
      SingleViewComponent,
      BasePushAlbumComponent,
      BaseFlashnoteComponent,
      HomeViewComponent,
      BaseFormComponent,
      MainHeaderComponent
    ],
    imports: [
      BrowserModule, FormsModule,ReactiveFormsModule, 

      // Inject HttpClientModule in the app
      HttpClientModule,

      // Inject router in the App
      RouterModule.forRoot( AppRouterModule,  { onSameUrlNavigation: 'reload' } )
    ],
    providers: [],
    bootstrap: [AppComponent]
  })
  export class AppModule { }
//