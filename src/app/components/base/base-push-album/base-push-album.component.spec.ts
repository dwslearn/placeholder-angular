import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BasePushAlbumComponent } from './base-push-album.component';

describe('BasePushAlbumComponent', () => {
  let component: BasePushAlbumComponent;
  let fixture: ComponentFixture<BasePushAlbumComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BasePushAlbumComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BasePushAlbumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
