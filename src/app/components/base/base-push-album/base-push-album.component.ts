/* 
  Imports
*/
  import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
//

/* 
  Definition and export
*/
  @Component({
    selector: 'app-base-push-album',
    template: `
      <div 
        class="app-base-push-album mb-4"
        *ngIf="singleitem"
      >
        <h2 class="title is-6 m-0 mb-1">{{ singleitem.title }}</h2>
        <button 
          class="button is-small is-primary"
          (click)="onClick()"
        >
          Voir les photos
        </button>
      </div>
    `
  })
  export class BasePushAlbumComponent implements OnInit {

    /* 
      [DATA] Binding
    */
      @Input() singleitem: { id: number, title: string }
      @Output() onDisplayAlbum: EventEmitter<{ id: number, title: string }> = new EventEmitter()
    //

    constructor() { }

    public onClick(): void {
      // Emit 'onDisplayAlbum' event
      this.onDisplayAlbum.emit( this.singleitem )
    }

    ngOnInit(): void {
    }
  }
//