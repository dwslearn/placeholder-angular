/* 
  Imports
*/
  // Angular component
  import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
//

/* 
  Defineition and export
*/
  @Component({
    selector: 'app-base-form',
    template: `
      <form 
        action="#"
        (submit)="prepareFormValue()"
      >
        <!-- Loop on form value -->
        <fieldset 
          class="mb-4"
          *ngFor="let item of formvalue"
        >
          <!-- Display label -->
          <label 
            class="label" 
            [for]="item.topic"
            [innerText]="item.label"

          ></label>

          <!-- Display inputs -->
          <div 
            *ngIf="['email', 'text'].indexOf(item.input.type) !== -1"
          >
            <input 
              class="input"
              min="5" 
              required
              [type]="item.input.type" 
              [name]="item.topic"
              [(ngModel)]="item.input.value"
            >
          </div>

          <div 
            *ngIf="['number'].indexOf(item.input.type) !== -1"
          >
            <input 
              class="input"
              type="number"
              min="1" 
              required
              [name]="item.topic"
              [(ngModel)]="item.input.value"
            >
          </div>
        </fieldset>

        <button
          class="button is-fullwidth is-primary"
          type="submit"
          [innerText]="textbutton"
          [disabled]="!formIsValide()"

        ></button>
      </form>
    `,
    styles: [
    ]
  })
  export class BaseFormComponent implements OnInit {
    /* 
      Declaration
    */
      @Input() formvalue: any
      @Input() textbutton: string
      @Output() onSubmit: EventEmitter<any> = new EventEmitter()
    //

    constructor() { }

    public prepareFormValue(){
      let formValue = {};
      for( let item of this.formvalue ){
        formValue[item.topic] = item.input.value
      }
      this.onSubmit.emit(formValue)
    }

    public formIsValide(){
      let isValid = true;
      for( let item of this.formvalue ){
        if( item.input.value === null ){
          isValid = false;
        }
      }

      return isValid
    }

    ngOnInit(): void {
    }

  }
//