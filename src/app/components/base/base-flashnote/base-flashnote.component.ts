/* 
  Imports
*/
  // Angular modules
  import { Component, OnInit, Output, EventEmitter } from '@angular/core';

  // Component module
  import { StateService } from "../../../services/state/state.service";
//

/* 
  Definition and export
*/
  @Component({
    selector: 'app-base-flashnote',
    template: `
      <article *ngIf="cmpFetchresponse">
        <div 
          class="modal"
          [ngClass]="{
            'is-active': cmpFetchresponse.action === 'displayimage'
          }"
        >
          <div class="modal-background"></div>
          <div class="modal-content">
            <figure class="box m-4">
              <img [src]="cmpFetchresponse.value.url" [alt]="cmpFetchresponse.value.title">
              <figcaption>{{cmpFetchresponse.value.title}}</figcaption>
            </figure>
          </div>
          <button 
            class="modal-close is-large" aria-label="close"
            (click)="onToggle.emit(true)"
          ></button>
        </div>
      </article>
    `,
  })
  export class BaseFlashnoteComponent implements OnInit {
    /* 
      Declaration
    */
      public cmpFetchresponse: any;
      @Output() onToggle: EventEmitter<Boolean> = new EventEmitter()
    //

    /* 
      [CMP] Injection
      Inject value inside the class
    */
      constructor( 
        private StateService: StateService,
      ) {
        // Subscribe to state mutations
        this.StateService.getFetchresponse().subscribe( (observer: any) => {
          this.cmpFetchresponse = observer
        })
      }
    //

    ngOnInit(): void {
    }

  }
//