import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BaseFlashnoteComponent } from './base-flashnote.component';

describe('BaseFlashnoteComponent', () => {
  let component: BaseFlashnoteComponent;
  let fixture: ComponentFixture<BaseFlashnoteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BaseFlashnoteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BaseFlashnoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
