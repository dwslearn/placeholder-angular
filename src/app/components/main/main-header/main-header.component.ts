/* 
  Imports
*/
  // Angular components
  import { Component, OnInit, Output, EventEmitter } from '@angular/core';

  // App components
  import { StateService } from "../../../services/state/state.service";
//

/* 
  Definition and export
*/
  @Component({
    selector: 'app-main-header',
    template: `
      <header class="has-background-primary p-4 is-size-6 is-uppercase has-text-white">
        <ul class="columns is-mobile">
          <!-- Public view -->
          <li class="column">Photo Album</li>
          <li
            class="column has-text-right" 
            *ngIf="cmpAuthinfo !== null"
          >
            <!-- Connected view -->
            <span>{{ cmpAuthinfo.email }}</span>
            <button 
              class="button is-small"
              [innerText]="'Logout'"
              (click)="onLogout.emit()"
            ></button>
          </li>
        </ul>
      </header>
    `,
  })
  export class MainHeaderComponent implements OnInit {
    /* 
      Declaration
    */
      // Define CMP properties
      public cmpAuthinfo: object;

      // Define CMP events
      @Output() onLogout: EventEmitter<null> = new EventEmitter();
    //

    /* 
      [CMP] Injection
      Use the constructor to add/inject value inside the CLASS
    */
      constructor( private StateService: StateService ) {
        // Subscribe to STATE mutation
        this.StateService.getAuthinfo().subscribe( (observer: object) => {
          // Update local values
          this.cmpAuthinfo = observer;
        })
      }
    //

    ngOnInit(): void {
    }

  }
//
/* 
# Création d'un header
Créer un composant MainHeader pour y ajouter les fonctionalités suivantes

## Non connecté
- Afficher titre APP

## Connecté
- Afficher titre APP
- Afficher email USER
- Afficher bouton "Logout" (https://bulma.io/documentation/elements/button/)

## Evénement
- Emettre depuis le HEADER un événement "onLogOut"
- Depuis le "App"|| "app.component" parent capter l'événement "onLogOut"
- Le composant "App"|| "app.component" déconnect le USER grâce au store/action/state
- Le USER déconnecté est renvoyer sur la "HomeView"
*/