/* 
  Imports
*/
  // Angular components
  import { Component, OnInit } from '@angular/core';
  import { ActivatedRoute } from "@angular/router";

  // CMP components
  import { CrudService } from "../../services/crud/crud.service";
  import { StateService } from "../../services/state/state.service";
//

/* 
  Definition and export
*/
  @Component({
    selector: 'app-single-view',
    template: `
      <article 
        class="colmn" 
        *ngIf="cpmSingleitem"
      >
        <!-- TODO: Create BaseImage -->
        <figure 
          class="box"
          *ngFor="let item of cpmSingleitem"
          (click)="onDisplayFull( item )"
        >
          <figcaption>{{ item.title }}</figcaption>
          <img [src]="item.thumbnailUrl" [alt]="item.title">
        </figure>
      </article>
    `
  })
  export class SingleViewComponent implements OnInit {
    /* 
      Declaration
    */
      public cpmSingleitem: object
    //

    /* 
      [CMP] Injection
      Inject value inside the class
    */
      constructor( 
        private CrudService: CrudService, 
        private StateService: StateService,
        private ActivatedRoute: ActivatedRoute 
      ) {
        // Get route params from oberser
        this.ActivatedRoute.params.subscribe( ( params: { endpoint: string, id: number } ) => {
          this.cmpRouterParams = params
        })

        // Subscribe to state mutations
        this.StateService.getSingleitem().subscribe( (observer: [object]) => {
          this.cpmSingleitem = observer
        })
      }
    //

    /* 
      [CMP] Declaration
      Initiate component properties
    */
      public cmpRouterParams: { endpoint: string, id: number };
    //

    /* 
      [CMP] Methods
      Define component methods
    */
      public onDisplayFull( event: object ): void {
        // Update state value to display flashnoye
        this.StateService.setStateValue('fetchresponse', {
          ok: true,
          action: 'displayimage',
          endpoint: undefined,
          value: event
        })
      }
    //


    /* 
      [HOOK] OnInit
      Call once, after the component is mounted
    */
      async ngOnInit() {
        /* 
          [CMP] Refresh
        */
          this.cpmSingleitem = null;
        //
        
        /* 
          [CRUD] Service
          Use the service to send HTTP request
        */
          await this.CrudService.readOperation({ 
            endpoint: `${this.cmpRouterParams.endpoint}?albumId=${this.cmpRouterParams.id}`, 
            state: 'singleitem' 
          })
        //
      }
    //
  }
//