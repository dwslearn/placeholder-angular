/* 
  Imports
*/
  // Angular components
  import { Component, OnInit } from '@angular/core';
  import { Router } from "@angular/router";
  
  // App component
  import { AuthService } from "../../services/auth/auth.service";
  import { StateService } from "../../services/state/state.service";
//

/* 
  Definition and export
*/
  @Component({
    selector: 'app-home-view',
    template: `
      <app-base-form
        *ngIf="cmpLoginForm"
        [formvalue]="cmpLoginForm"
        [textbutton]="'Connexion'"
        (onSubmit)="onLogin($event)"
      ></app-base-form>
    `,
  })
  export class HomeViewComponent implements OnInit {
    /* 
      Declaration
    */
      public cmpLoginForm: any;
    //

    /* 
      [CMP] Constructor
      Inject values
    */
      constructor( 
        private AuthService: AuthService, 
        private Router: Router, 
        private StateService: StateService 
      ) {
        // Define login form value
        this.cmpLoginForm = [
          {
            _id: 0,
            topic: 'email',
            label: 'Votre email',
            input: {
              type: 'email',
              value: 'Sincere@april.biz',
            }
          },
          {
            _id: 1,
            topic: 'id',
            label: 'Votre ID',
            input: {
              type: 'number',
              value: 1,
            }
          }
        ]

        // Subscribe to state mutations
        this.StateService.getAuthinfo().subscribe( (observer: [object]) => {
          // Checl cmpAuthinfo value
          if(observer !== null){
            this.Router.navigate( ['/dashboard'] )
          }
        })
      }
    //

    public onLogin(event: {email: string, id: number}){
      // Use AUTH service to log user
      this.AuthService.loginOperation( event )
    }

    ngOnInit(): void {
    }

  }
//