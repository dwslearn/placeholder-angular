/* 
  Imports
*/
  // Angular components
  import { Component, OnInit } from '@angular/core';
  import { Router } from "@angular/router";

  // CMP components
  import { CrudService } from "../../services/crud/crud.service";
  import { StateService } from "../../services/state/state.service";
//

/* 
  Definition and export
*/
  @Component({
    selector: 'app-dashboard-view',
    template: `
      <section 
        class="app-dashboard-view"
        *ngIf="cmpAlbumlist.length"
      >
        <article 
          *ngFor="let item of cmpAlbumlist"
        >
          <app-base-push-album
            [singleitem]="item"
            (onDisplayAlbum)="onDisplayAlbum($event)"
          >
          </app-base-push-album>
        </article>
      </section>
    `
  })
  export class DashboardViewComponent implements OnInit {
    /* 
      Declaration
    */
      public cmpAlbumlist: [object];
    //

    /* 
      [CMP] Injection
      Inject value inside the class
    */
      constructor( 
        private CrudService: CrudService, 
        private StateService: StateService,
        private Router: Router
      ) {
        // Subscribe to state mutations
        this.StateService.getAlbumlist().subscribe( (observer: [object]) => {
          this.cmpAlbumlist = observer
        })

        // TODO: redirect unconnected user to HomeView
      }
    //

    /* 
      [CMP] Method
      Set of functionalities
    */
      // Change route to display album view
      public onDisplayAlbum( event: { id: number, title: string } ): void {
        // Change user route
        this.Router.navigateByUrl(`/photos/${event.id}`);
      }
    //

    /* 
      [HOOK] OnInit
      Call once, after the component is mounted
    */
      async ngOnInit() {
        /* 
          [CRUD] Service
          Use the service to send HTTP request
        */
          await this.CrudService.readOperation({ endpoint: 'albums?userId=1', state: 'albumlist' })
        //
      }
    //
  }
//